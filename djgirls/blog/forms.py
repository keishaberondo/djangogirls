from django.shortcuts import render
from django.utils import timezone
from django import forms


from .models import Post, User, Profile

#
# class UserForm(forms.ModelForm):
#     password = forms.CharField(widget=forms.PasswordInput)
#     class Meta:
#         model = User
#         fields = ('email', 'password',)
#
#
# class ProfileForm(forms.ModelForm):
#     date_of_birth = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
#     class Meta:
#         model = Profile
#         fields = ('name', 'birthdate',)


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text',)