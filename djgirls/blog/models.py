from django.db import models
from django.utils import timezone
# Create your models here.


class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class User(models.Model):
    email = models.EmailField(max_length=30)
    password = models.CharField(max_length=32, null=False, blank=False)


class Profile(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    birthdate = models.DateField(null=False, blank=False)
