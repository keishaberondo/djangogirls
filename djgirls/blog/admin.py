from django.conf.urls import url
from django.contrib import admin
from .models import User, Profile, Post
# Register your models here.

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    admin.site.register(User),
    admin.site.register(Profile),
    admin.site.register(Post)
]
